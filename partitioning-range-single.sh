#!/bin/sh

PARTITIONS=10000	# number of partitions (per level)
ROWS=1000			# number of rows per partition
LEVELS=1			# levels of partitioning

ROWS_TOTAL=$((PARTITIONS * ROWS))

function do_sql {
	echo $1 ";"
}

if [ "$1" -eq "1" ]; then
	do_sql "BEGIN"
fi

do_sql "CREATE TABLE range_test_single (a INT, val INT) PARTITION BY RANGE (a)"

for p1 in `seq 1 $PARTITIONS`; do

	s=$(((p1-1) * ROWS))
	e=$((p1 * ROWS))

	do_sql "CREATE TABLE range_test_single_$p1 PARTITION OF range_test_single FOR VALUES FROM ($s) TO ($e)"

	do_sql "ALTER TABLE range_test_single_${p1} ADD PRIMARY KEY (a)"

done

do_sql "INSERT INTO range_test_single SELECT i, i FROM generate_series(0, $ROWS_TOTAL -1) s(i)"

if [ "$1" -eq "1" ]; then
	do_sql "COMMIT"
fi

do_sql "VACUUM ANALYZE"
