#!/bin/sh

PARTITIONS=100		# number of partitions (per level)
ROWS=1000			# number of rows per partition
KEYS=2				# keys per leaf partition
LEVELS=2			# levels of partitioning

PARTITIONS_TOTAL=1
for i in `seq 1 $LEVELS`; do
	PARTITIONS_TOTAL=$((PARTITIONS_TOTAL * PARTITIONS))
done

ROWS_TOTAL=$((PARTITIONS_TOTAL * ROWS))
KEYS_TOTAL=$((PARTITIONS_TOTAL * KEYS))

KEYS_TOP=$((KEYS*PARTITIONS))

function do_sql {
	echo $1 ";"
}

if [ "$1" -eq "1" ]; then
	do_sql "BEGIN"
fi

do_sql "CREATE TABLE list_test_multi (a INT, val INT) PARTITION BY LIST (a)"

for p1 in `seq 1 $PARTITIONS`; do

	s=$(((p1-1) * KEYS_TOP + 1))
	e=$((p1 * KEYS_TOP))

	l=`seq -s ' ' $s $e | sed "s/ /, /g"`

	do_sql "CREATE TABLE list_test_multi_${p1} PARTITION OF list_test_multi FOR VALUES IN ($l) PARTITION BY LIST (a)"

	for p2 in `seq 1 $PARTITIONS`; do

		s2=$((s + (p2-1)*KEYS))
		e2=$((s2 + KEYS - 1))

		l=`seq -s ' ' $s2 $e2 | sed "s/ /, /g"`

		do_sql "CREATE TABLE list_test_multi_${p1}_${p2} PARTITION OF list_test_multi_$p1 FOR VALUES IN ($l)"

		do_sql "CREATE INDEX ON list_test_multi_${p1}_${p2} (a)"

	done

done

do_sql "INSERT INTO list_test_multi SELECT mod(i, $KEYS_TOTAL) + 1, i FROM generate_series(0, $ROWS_TOTAL -1) s(i)"

if [ "$1" -eq "1" ]; then
	do_sql "COMMIT"
fi

do_sql "VACUUM ANALYZE"
