#!/bin/sh

PARTITIONS=100		# number of partitions (per level)
ROWS=1000			# number of rows per partition
LEVELS=2			# levels of partitioning

PARTITIONS_TOTAL=1
for i in `seq 1 $LEVELS`; do
	PARTITIONS_TOTAL=$((PARTITIONS_TOTAL * PARTITIONS))
done

ROWS_TOTAL=$((PARTITIONS_TOTAL * ROWS))
TOP_COUNT=$((ROWS * PARTITIONS))

function do_sql {
	echo $1 ";"
}

if [ "$1" -eq "1" ]; then
	do_sql "BEGIN"
fi

do_sql "CREATE TABLE range_test_multi (a INT, val INT) PARTITION BY RANGE (a)"

for p1 in `seq 1 $PARTITIONS`; do

	s=$(((p1-1) * TOP_COUNT))
	e=$((p1 * TOP_COUNT))

	do_sql "CREATE TABLE range_test_multi_$p1 PARTITION OF range_test_multi FOR VALUES FROM ($s) TO ($e) PARTITION BY RANGE (a)"

	for p2 in `seq 1 $PARTITIONS`; do

		s2=$((s + (p2-1) * ROWS))
		e2=$((s2 + ROWS))

		do_sql "CREATE TABLE range_test_multi_${p1}_${p2} PARTITION OF range_test_multi_$p1 FOR VALUES FROM ($s2) TO ($e2)"

		do_sql "ALTER TABLE range_test_multi_${p1}_${p2} ADD PRIMARY KEY (a)"

	done

done

do_sql "INSERT INTO range_test_multi SELECT i, i FROM generate_series(0, $ROWS_TOTAL -1) s(i)"

if [ "$1" -eq "1" ]; then
	do_sql "COMMIT"
fi

do_sql "VACUUM ANALYZE"
