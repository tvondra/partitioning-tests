#!/bin/sh

PARTITIONS=10000	# number of partitions (per level)
ROWS=1000			# number of rows per partition
KEYS=10				# keys per leaf partition
LEVELS=1			# levels of partitioning

ROWS_TOTAL=$((PARTITIONS * ROWS))
KEYS_TOTAL=$((PARTITIONS * KEYS))

function do_sql {
	echo $1 ";"
}

if [ "$1" -eq "1" ]; then
	do_sql "BEGIN"
fi

do_sql "CREATE TABLE list_test_single (a INT, val INT) PARTITION BY LIST (a)"

for p1 in `seq 1 $PARTITIONS`; do

	s=$(((p1-1) * KEYS + 1))
	e=$((p1 * KEYS))

	l=`seq -s ' ' $s $e | sed "s/ /, /g"`

	do_sql "CREATE TABLE list_test_single_${p1} PARTITION OF list_test_single FOR VALUES IN ($l)"

	do_sql "CREATE INDEX ON list_test_single_${p1} (a)"

done

do_sql "INSERT INTO list_test_single SELECT mod(i, $KEYS_TOTAL) + 1, i FROM generate_series(1, $ROWS_TOTAL) s(i)"

if [ "$1" -eq "1" ]; then
	do_sql "COMMIT"
fi

do_sql "VACUUM ANALYZE"
