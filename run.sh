#!/bin/sh

for s in range-single range-multi range-list list-single list-range list-multi; do

	echo `date +%s` $s "recreate database"
	dropdb --if-exists $s
	createdb $s

	echo `date +%s` $s "create partitions"

	./partitioning-$s.sh 0 | tee $s.sql | psql $s > $s.log 2>&1

	echo `date +%s` $s "pgbench simple"

	pgbench -n -c 4 -T 300 -f pgbench-$s.sql $s > pgbench-$s-simple.log 2>&1

	echo `date +%s` $s "pgbench prepared"

	pgbench -n -c 4 -T 300 -M prepared -f pgbench-$s.sql $s > pgbench-$s-prepared.log 2>&1

	echo `date +%s` $s "done"

done
