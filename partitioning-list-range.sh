#!/bin/sh

PARTITIONS=100		# number of partitions (per level)
ROWS=1000			# number of rows per partition
KEYS=10				# keys per leaf partition
LEVELS=2			# levels of partitioning

PARTITIONS_TOTAL=1
for i in `seq 1 $LEVELS`; do
	PARTITIONS_TOTAL=$((PARTITIONS_TOTAL * PARTITIONS))
done

ROWS_TOTAL=$((PARTITIONS_TOTAL * ROWS))

function do_sql {
	echo "$1" ";"
}

if [ "$1" -eq "1" ]; then
	do_sql "BEGIN"
fi

do_sql "CREATE TABLE list_range_test (a INT, b INT, val INT) PARTITION BY LIST (a)"

for p1 in `seq 1 $PARTITIONS`; do

	s=$(((p1-1) * KEYS + 1))
	e=$((p1 * KEYS))

	l=`seq -s ' ' $s $e | sed "s/ /, /g"`

	do_sql "CREATE TABLE list_range_test_${p1} PARTITION OF list_range_test FOR VALUES IN ($l) PARTITION BY RANGE (b)"

	for p2 in `seq 1 $PARTITIONS`; do

		s2=$(((p2-1)*KEYS))
		e2=$(((p2  )*KEYS))

		do_sql "CREATE TABLE list_range_test_${p1}_${p2} PARTITION OF list_range_test_$p1 FOR VALUES FROM ($s2) TO ($e2)"

		do_sql "CREATE INDEX ON list_range_test_${p1}_${p2} (a)"

	done

done

do_sql "INSERT INTO list_range_test SELECT mod(($KEYS * $PARTITIONS * random())::int, $KEYS * $PARTITIONS) + 1, mod(i, $KEYS) + 1, i FROM generate_series(0, $ROWS_TOTAL -1) s(i)"

if [ "$1" -eq "1" ]; then
	do_sql "COMMIT"
fi

do_sql "VACUUM ANALYZE"
