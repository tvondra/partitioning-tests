#!/bin/sh

PARTITIONS=100		# number of partitions (per level)
ROWS=1000			# number of rows per partition
KEYS=10				# keys per leaf partition
LEVELS=2			# levels of partitioning

PARTITIONS_TOTAL=1
for i in `seq 1 $LEVELS`; do
	PARTITIONS_TOTAL=$((PARTITIONS_TOTAL * PARTITIONS))
done

ROWS_TOTAL=$((PARTITIONS_TOTAL * ROWS))

function do_sql {
	echo "$1" ";"
}

if [ "$1" -eq "1" ]; then
	do_sql "BEGIN"
fi

do_sql "CREATE TABLE range_list_test (a INT, b INT, val INT) PARTITION BY RANGE (a)"

for p1 in `seq 1 $PARTITIONS`; do

	s=$(((p1-1)*KEYS))
	e=$(((p1  )*KEYS))

	do_sql "CREATE TABLE range_list_test_${p1} PARTITION OF range_list_test FOR VALUES FROM ($s) TO ($e) PARTITION BY LIST (b)"

	for p2 in `seq 1 $PARTITIONS`; do

		s2=$(((p2-1) * KEYS + 1))
		e2=$((p2 * KEYS))

		l=`seq -s ' ' $s2 $e2 | sed "s/ /, /g"`

		do_sql "CREATE TABLE range_list_test_${p1}_${p2} PARTITION OF range_list_test_${p1} FOR VALUES IN ($l)"

		do_sql "CREATE INDEX ON range_list_test_${p1}_${p2} (a, b)"

	done

done

do_sql "INSERT INTO range_list_test SELECT mod(i, $KEYS) + 1, mod(($KEYS * $PARTITIONS * random())::int, $KEYS * $PARTITIONS) + 1, i FROM generate_series(0, $ROWS_TOTAL -1) s(i)"

if [ "$1" -eq "1" ]; then
	do_sql "COMMIT"
fi

do_sql "VACUUM ANALYZE"
